SRC_DIR=src
BIN_DIR=target/linux
INCLUDE_DIR=include

CC=gcc
CFLAGS=-I$(SRC_DIR) -I$(INCLUDE_DIR) -Wall -Wextra -Wformat -Wshadow
LIBS=-ldl -lpthread -lX11 -lXi

MODULES=warrior migration db util log \
	keys/keys keys/defs \
	stats/stats stats/by_key stats/by_day
HEADERS=const
INCLUDES=sqlite3

DEPS=$(patsubst %,$(SRC_DIR)/%.h,$(MODULES) $(HEADERS)) $(patsubst %,$(INCLUDE_DIR)/%.h,$(INCLUDES))
OBJ=$(patsubst %,$(BIN_DIR)/%.o,$(MODULES)) $(patsubst %,$(BIN_DIR)/include/%.o,$(INCLUDES))

NAME=warrior

all: $(NAME)
$(NAME): $(BIN_DIR)/$(NAME)
$(BIN_DIR)/$(NAME): $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LIBS)

$(BIN_DIR)/%.o: $(SRC_DIR)/%*.c $(DEPS)
	@mkdir -p $(@D)
	$(CC) -c -o $@ $< $(CFLAGS)
$(BIN_DIR)/include/%.o: $(INCLUDE_DIR)/%.c
	@mkdir -p $(@D)
	$(CC) -c -o $@ $< $(CFLAGS)

clean:
	rm -rf $(BIN_DIR)

debug:
	$(MAKE) "BUILD=debug"

release:
	$(MAKE) "BUILD=release"

windows:
	$(MAKE) "CC=/usr/bin/x86_64-w64-mingw32-gcc" "LIBS=-static -lpthread" "NAME=warrior.exe" "BIN_DIR=target/windows"

ifeq ($(BUILD),debug)
CFLAGS += -O0 -g -DDEBUG -fsanitize=address -fsanitize=undefined -fno-omit-frame-pointer
else ifeq ($(BUILD),release)
CFLAGS += -O2
endif
