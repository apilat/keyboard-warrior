#ifndef WARRIOR_KEYS_DEFS
#define WARRIOR_KEYS_DEFS

#include <stdint.h>
#include <stdbool.h>

typedef uint16_t keycode;
const char* keycode_to_string(keycode key);
keycode keycode_from_x11_keysym(uint32_t keysym);
keycode keycode_from_win_virtual_code(uint8_t code, bool shift);

#endif
