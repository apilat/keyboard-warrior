#ifndef WARRIOR_KEYS
#define WARRIOR_KEYS

#include "defs.h"

#include <unistd.h>
#include <stdint.h>

struct database;

struct key_aggregator;
struct key_aggregator* key_aggregator_init(struct database *db, size_t buffer_capacity);
int key_aggregator_record_keyboard(struct key_aggregator *obj, keycode code);
int key_aggregator_record_mouse(struct key_aggregator *obj, int button, double x, double y);
void key_aggregator_free(struct key_aggregator* obj);

struct key_ingestor;
struct key_ingestor* key_ingestor_init(struct key_aggregator* agg, bool keyboard, bool mouse);
void key_ingestor_free(struct key_ingestor* obj);

#endif
