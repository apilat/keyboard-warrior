#include "../log.h"
#include "../util.h"
#include "keys.h"

#include <X11/XKBlib.h>
#include <X11/Xlib.h>
#include <X11/extensions/XI2.h>
#include <X11/extensions/XInput2.h>
#include <errno.h>
#include <poll.h>
#include <pthread.h>
#include <setjmp.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

struct key_ingestor {
    bool on_keyboard, on_mouse;
    int txfd, rxfd;
    pthread_t worker;
    struct key_aggregator *agg;
};

struct x_data;
static int worker_main(struct key_ingestor *);
static void handle_event(struct key_ingestor *, struct x_data *, XEvent *);
static int init_x(struct x_data *);

struct key_ingestor *key_ingestor_init(struct key_aggregator *agg, bool on_keyboard,
                                       bool on_mouse) {
    debug("initializing key ingestor (X11)");
    struct key_ingestor *keys = malloc_safe(sizeof(*keys));

    int pipefd[2];
    if (pipe(pipefd)) {
        error("pipe failed: %s", strerror(errno));
        goto error0;
    }
    keys->on_keyboard = on_keyboard;
    keys->on_mouse = on_mouse;
    keys->rxfd = pipefd[0];
    keys->txfd = pipefd[1];

    keys->agg = agg;

    if (pthread_create(&keys->worker, NULL, (void *)worker_main, keys)) {
        goto error1;
    }

    return keys;

error1:
    free(keys);
error0:
    close(pipefd[0]);
    close(pipefd[1]);
    return NULL;
}

void key_ingestor_free(struct key_ingestor *ing) {
    debug("freeing key ingestor (X11)");

    if (write(ing->txfd, "0", 1) != 1) {
        error("write to worker failed: %s, forcing shutdown", strerror(errno));
        pthread_cancel(ing->worker);
    }
    pthread_join(ing->worker, NULL);
    if (close(ing->txfd)) {
        warn("close tx failed: %s", strerror(errno));
    }
    if (close(ing->rxfd)) {
        warn("close rx failed: %s", strerror(errno));
    }

    free(ing);
}

_Thread_local jmp_buf x11_error_env;

static int io_error_handler(Display *display) {
    (void)display;
    longjmp(x11_error_env, 1);
}

static int std_error_handler(Display *display, XErrorEvent *err) {
    char buf[256] = {0};
    XGetErrorText(display, err->error_code, buf, sizeof(buf));
    error("non-fatal x error: %s", buf);
    return 0;
}

struct x_data {
    Display *display;
    int xinput_ext_code, xkb_ext_code;
    int xkb_core_state;
};

static int init_x(struct x_data *x) {
    debug("opening x display");
    x->display = XOpenDisplay(NULL);
    if (!x->display) {
        warn("failed to open display");
        goto error1;
    }

    debug("checking for xinput");
    int _empty;
    if (!XQueryExtension(x->display, "XInputExtension", &x->xinput_ext_code, &_empty, &_empty)) {
        error("XInput is not supported");
        goto error2;
    }
    debug("verifying xinput version >= 2.2");
    int major = 2, minor = 2;
    if (XIQueryVersion(x->display, &major, &minor) != Success) {
        error("XI2 is not supported");
        goto error2;
    }

    debug("checking for xkb");
    if (!XkbQueryExtension(x->display, &_empty, &x->xkb_ext_code, &_empty, &_empty, &_empty)) {
        error("XKB is not supported");
        goto error2;
    }

    XIEventMask evmasks[2];
    unsigned char mask1[(XI_LASTEVENT + 7) / 8] = {0}, mask2[(XI_LASTEVENT + 7) / 8] = {0};
    XISetMask(mask1, XI_RawKeyPress);
    evmasks[0] =
        (XIEventMask){.mask = mask1, .mask_len = sizeof(mask1), .deviceid = XIAllMasterDevices};
    // Need to use separate masks here to avoid double-registering keys from slave devices
    XISetMask(mask2, XI_ButtonPress);
    evmasks[1] = (XIEventMask){.mask = mask2, .mask_len = sizeof(mask2), .deviceid = XIAllDevices};

    debug("registering event listeners");
    if (XISelectEvents(x->display, DefaultRootWindow(x->display), evmasks, 2) != Success) {
        error("XISelectEvents failed");
        goto error2;
    }

    debug("registering StateNotify listener");
    if (!XkbSelectEvents(x->display, XkbUseCoreKbd, XkbStateNotifyMask, XkbStateNotifyMask)) {
        error("XkbSelectEvents failed");
        goto error2;
    }

    debug("flushing all commands to server");
    if (!XFlush(x->display)) {
        error("XFlush failed");
        goto error2;
    }

    x->xkb_core_state = 0;

    return 0;

error2:
    debug("closing x connection");
    if (XCloseDisplay(x->display) != Success) {
        warn("XCloseDisplay failed");
    }
    x->display = NULL;
error1:
    return 1;
}

static int worker_main(struct key_ingestor *ing) {
    info("worker starting");

    debug("disabling signal handling");
    // We need to block this thread from receiving signals to make sure that
    // the main thread receives all of them and can initiate a shutdown.
    if (ignore_signals_pthread()) {
        warn("failed to disable signal handling: %s", strerror(errno));
        warn("we might not be able to shut down cleanly on signals");
    }

    debug("setting x error handlers");
    XSetErrorHandler(std_error_handler);
    if (setjmp(x11_error_env)) {
        warn("fatal x error occured, likely because the server shut down");
    } else {
        XSetIOErrorHandler(io_error_handler);
    }

    int x_conn_timeout = 1;
    struct pollfd fds[2] = {
        {.fd = ing->rxfd, .events = POLLIN},
    };
    while (1) {
        struct x_data x;
        if (init_x(&x)) {
            warn("failed to initialize x connection, we will attempt again in %d seconds",
                 x_conn_timeout);

            int ret = poll(fds, 1, x_conn_timeout * 1000);
            if (ret < 0) {
                error("poll failed: %s, treating this as fatal", strerror(errno));
                goto exit;
            }
            if (fds[0].revents & POLLIN) {
                goto exit_message;
            }

            x_conn_timeout *= 2;
            if (x_conn_timeout > 60) {
                x_conn_timeout = 60;
            }
            continue;
        }

        x_conn_timeout = 1;
        fds[1] = (struct pollfd){.fd = ConnectionNumber(x.display), .events = POLLIN};

        while (1) {
            int ret = poll(fds, 2, -1);
            if (ret < 0) {
                warn("poll failed: %s", strerror(errno));
                break;
            }

            if (fds[0].revents & POLLIN) {
                goto exit_message;
            }

            if (fds[1].revents & POLLIN) {
                trace("event received");
                XEvent ev;
                XNextEvent(x.display, &ev);
                handle_event(ing, &x, &ev);
            }
        }
    }

exit_message:
    debug("shutdown message received");
    char buf[1];
    if (read(ing->rxfd, buf, sizeof(buf)) != 1) {
        error("read from master failed: %s, exiting anyway", strerror(errno));
    }

exit:
    info("worker exiting");
    return 0;
}

static void handle_event(struct key_ingestor *ing, struct x_data *x, XEvent *ev) {
    XGenericEventCookie *cookie = &ev->xcookie;

    if (ev->type == x->xkb_ext_code) {
        XkbEvent *xev = (XkbEvent *)&ev;
        if (xev->any.xkb_type == XkbStateNotify) {
            XkbStateNotifyEvent sev = xev->state;
            x->xkb_core_state = XkbBuildCoreState(sev.mods, sev.group);
            trace("notify mods=%d group=%d", sev.mods, sev.group);
        }

    } else if (ev->type == GenericEvent && cookie->extension == x->xinput_ext_code) {
        if (cookie->evtype == XI_RawKeyPress) {
            if (!ing->on_keyboard) {
                return;
            }

            if (!XGetEventData(x->display, cookie)) {
                warn("failed to get extra data for xinput cookie, dropping");
                return;
            }
            XIRawEvent *rev = (XIRawEvent *)cookie->data;
            int raw_keycode = rev->detail;

            trace("press keycode=%d", raw_keycode);
            KeySym keysym;
            if (!XkbLookupKeySym(x->display, raw_keycode, x->xkb_core_state, NULL, &keysym)) {
                warn("failed to lookup keysym for %ld, dropping", raw_keycode);
                return;
            }
            keycode code = keycode_from_x11_keysym(keysym);
            if (code == 0) {
                warn("keysym %d doesn't have a defined keycode", keysym);
            }
            if (key_aggregator_record_keyboard(ing->agg, code)) {
                warn("failed to save keypress");
            }

            XFreeEventData(x->display, cookie);

        } else if (cookie->evtype == XI_ButtonPress) {
            if (!ing->on_mouse) {
                return;
            }

            if (!XGetEventData(x->display, cookie)) {
                warn("failed to get extra data for xinput cookie, dropping");
                return;
            }
            XIDeviceEvent *rev = (XIDeviceEvent *)cookie->data;
            int button = rev->detail;
            double px = rev->event_x, py = rev->event_y;

            trace("mouse button=%d x=%lf y=%lf", button, px, py);

            // Accept left, middle, right and two side buttons on my mouse.
            // We explicitly don't want 4 and 5 - these are scroll events which would flood the
            // database if allowed.
            static const int accepted_buttons[] = {1, 2, 3, 8, 9};

            bool accepted = false;
            for (size_t i = 0; i < ARRAY_SIZE(accepted_buttons); i++) {
                if (button == accepted_buttons[i]) {
                    accepted = true;
                    break;
                }
            }

            if (accepted) {
                if (key_aggregator_record_mouse(ing->agg, button, px, py)) {
                    warn("failed to save mouse press");
                }
            }

            XFreeEventData(x->display, cookie);
        }
    }
}
