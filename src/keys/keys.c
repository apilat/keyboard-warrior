#include "keys.h"
#include "defs.h"

#ifdef __linux__
#include "ingestor_x11.c"

#elif _WIN64
#include "ingestor_winapi.c"

#else
#warning "key ingestor unsupported on this os"

#include "../log.h"
struct key_ingestor {};
struct key_ingestor *key_ingestor_init(struct key_aggregator *agg, bool keyboard, bool mouse) {
    (void)agg;
    (void)keyboard;
    (void)mouse;
    error("key scanner unsupported");
    return NULL;
}
void key_ingestor_free(struct key_ingestor *obj) { (void)obj; }

#endif

#include "../const.h"
#include "../db.h"

#include <errno.h>
#include <sqlite3.h>

struct key_aggregator {
    struct database *db; // nullable

    union event *buf;
    size_t buf_size, buf_cap;
    // The buffer capacity can grow beyond the flush threshold if we repeatedly fail to flush.
    size_t flush_threshold;
};

struct key_aggregator *key_aggregator_init(struct database *db, size_t flush_threshold) {
    debug("initializing key aggregator");

    if (flush_threshold < 1 || flush_threshold > 16384) {
        error("buffer capacity must be between 1 and 16384");
        return NULL;
    }

    struct key_aggregator *obj = malloc_safe(sizeof(*obj));
    obj->db = db;

    obj->buf = malloc_safe(flush_threshold * sizeof(obj->buf[0]));
    obj->buf_size = 0;
    obj->buf_cap = flush_threshold;
    obj->flush_threshold = flush_threshold;

    return obj;
}

static long long get_randomized_time() {
    struct timespec tv;
    if (clock_gettime(CLOCK_REALTIME, &tv)) {
        error("clock_gettime failed: %s", strerror(errno));
        return 0;
    }

    int offset = randint() % (MAX_TIME_OFFSET_MS * 2000) - MAX_TIME_OFFSET_MS * 1000;
    return tv.tv_sec * 1000000 + tv.tv_nsec / 1000 + offset;
}

static double get_randomized_pos(double x) {
    double offset = (double)(randint() % (MOUSE_RANDOM_PIXEL_OFFSET_MAX * 2 * 1000)) / 1000.0 -
                    MOUSE_RANDOM_PIXEL_OFFSET_MAX;
    return x + offset;
}

static int flush_buffer(struct key_aggregator *obj) {
    debug("flushing buffer");

    if (obj->db) {
        int ret = database_insert_batch(obj->db, obj->buf, obj->buf_size);
        if (ret == 0) {
            memset(obj->buf, 0, obj->buf_size * sizeof(*obj->buf));
            obj->buf_size = 0;
        }
        return ret;
    } else {
        for (size_t i = 0; i < obj->buf_size; i++) {
            union event evt = obj->buf[i];
            switch (evt.type) {
            case EVT_KEY:
                printf("type=key timestamp=%ld keycode=%d (%s)\n", evt.key.timestamp,
                       evt.key.keycode, keycode_to_string(evt.key.keycode));
                break;

            case EVT_MOUSE:
                printf("type=mouse timestamp=%ld button=%d x=%lf y=%lf\n", evt.key.timestamp,
                       evt.mouse.button, evt.mouse.x, evt.mouse.y);
                break;
            }
        }
        obj->buf_size = 0;
        return 0;
    }
}

static int key_buffer_push(struct key_aggregator *obj, union event *evt) {
    buffer_push((void **)&obj->buf, sizeof(obj->buf[0]), &obj->buf_size, &obj->buf_cap, evt);
    size_t j = randint() % obj->buf_size;
    union event tmp = obj->buf[j];
    obj->buf[j] = obj->buf[obj->buf_size - 1];
    obj->buf[obj->buf_size - 1] = tmp;

    if (obj->buf_size >= obj->flush_threshold) {
        int ret = flush_buffer(obj);
        if (ret) {
            warn("failed to flush buffer");
        }
        return ret;
    }

    return 0;
}

int key_aggregator_record_keyboard(struct key_aggregator *obj, keycode code) {
    trace("recording keycode=%d", code);

    union event evt;
    evt.type = EVT_KEY;
    evt.key.timestamp = get_randomized_time();
    evt.key.keycode = code;

    int ret;
    if ((ret = key_buffer_push(obj, &evt))) {
        return ret;
    }

    return 0;
}

int key_aggregator_record_mouse(struct key_aggregator *obj, int button, double x, double y) {
    trace("recording mouse press button=%d x=%lf y=%lf", button, x, y);

    union event evt;
    evt.type = EVT_MOUSE;
    evt.key.timestamp = get_randomized_time();
    // Could randomize the button with some probability too but probably unnecessary. Mouse clicks
    // and sequences are definitely not as sensitive as key presses.
    evt.mouse.button = button;
    evt.mouse.x = get_randomized_pos(x);
    evt.mouse.y = get_randomized_pos(y);

    int ret;
    if ((ret = key_buffer_push(obj, &evt))) {
        return ret;
    }

    return 0;
}

void key_aggregator_free(struct key_aggregator *obj) {
    debug("freeing key aggregator");

    if (flush_buffer(obj)) {
        warn("failed to flush buffer while exiting");
    }

    free(obj->buf);
    free(obj);
}
