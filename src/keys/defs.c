#include "defs.h"

#include <stddef.h>

const char *keycode_to_string(keycode key) {
    const uint8_t high = key >> 8, low = key & 0xff;

    if (high == 0) {
        if (low == 0) {
            return "Unknown";
        } else if (low == 32) {
            return "Space";
        } else if (32 < low && low < 127) {
            const char *alphabet =
                " \000!\000\"\000#\000$\000%\000&\000'\000(\000)\000*\000+\000,\000-\000.\000/"
                "\0000\0001\0002\0003\0004\0005\0006\0007\0008\0009\000:\000;\000<\000=\000>\000?"
                "\000@"
                "\000A\000B\000C\000D\000E\000F\000G\000H\000I\000J\000K\000L\000M\000N\000O\000P"
                "\000Q\000R\000S\000T\000U\000V\000W\000X\000Y\000Z\000[\000\\\000]\000^\000_\000`"
                "\000a\000b\000c\000d\000e\000f\000g\000h\000i\000j\000k\000l\000m\000n\000o\000p"
                "\000q\000r\000s\000t\000u\000v\000w\000x\000y\000z\000{\000|\000}\000~\000";
            return &alphabet[(low - 32) * 2];
        }

    } else if (high == 1) {
        switch (low) {
        case 1:
            return "ShiftL";
        case 2:
            return "ShiftR";
        case 3:
            return "CtrlL";
        case 4:
            return "CtrlR";
        case 5:
            return "AltL";
        case 6:
            return "AltR";
        case 7:
            return "SuperL";
        case 8:
            return "SuperR";
        case 9:
            return "Backspace";
        case 10:
            return "Enter";
        case 11:
            return "Tab";

        case 12:
            return "F1";
        case 13:
            return "F2";
        case 14:
            return "F3";
        case 15:
            return "F4";
        case 16:
            return "F5";
        case 17:
            return "F6";
        case 18:
            return "F7";
        case 19:
            return "F8";
        case 20:
            return "F9";
        case 21:
            return "F10";
        case 22:
            return "F11";
        case 23:
            return "F12";
        case 24:
            return "F13";
        case 25:
            return "F14";
        case 26:
            return "F15";
        case 27:
            return "F16";
        case 28:
            return "F17";
        case 29:
            return "F18";
        case 30:
            return "F19";
        case 31:
            return "F20";
        case 32:
            return "F21";
        case 33:
            return "F22";
        case 34:
            return "F23";
        case 35:
            return "F24";

        case 36:
            return "Escape";
        }

    } else if (high == 2) {
        switch (low) {
        case 1:
            return "PrintScreen";
        case 2:
            return "ScrollLock";
        case 3:
            return "Pause";
        case 4:
            return "Insert";
        case 5:
            return "Home";
        case 6:
            return "Delete";
        case 7:
            return "End";
        case 8:
            return "PageUp";
        case 9:
            return "PageDown";
        case 10:
            return "Compose";
        case 11:
            return "Menu";

        case 12:
            return "ArrowLeft";
        case 13:
            return "ArrowRight";
        case 14:
            return "ArrowUp";
        case 15:
            return "ArrowDown";

        case 16:
            return "KP_Enter";
        case 17:
            return "KP_Home";
        case 18:
            return "KP_Left";
        case 19:
            return "KP_Up";
        case 20:
            return "KP_Right";
        case 21:
            return "KP_Down";
        case 22:
            return "KP_PageDown";
        case 23:
            return "KP_PageUp";
        case 24:
            return "KP_End";
        case 25:
            return "KP_Begin";
        case 26:
            return "KP_Insert";
        case 27:
            return "KP_Delete";

        case 29:
            return "KP_Multiply";
        case 30:
            return "KP_Add";
        case 31:
            return "KP_Separator";
        case 32:
            return "KP_Subtract";
        case 33:
            return "KP_Decimal";
        case 34:
            return "KP_Divide";
        case 35:
            return "KP_0";
        case 36:
            return "KP_1";
        case 37:
            return "KP_2";
        case 38:
            return "KP_3";
        case 39:
            return "KP_4";
        case 40:
            return "KP_5";
        case 41:
            return "KP_6";
        case 42:
            return "KP_7";
        case 43:
            return "KP_8";
        case 44:
            return "KP_9";

        case 45:
            return "NumLock";
        case 46:
            return "CapsLock";
        }

    } else if (high == 3) {
        switch (low) {
        case 1:
            return "AudioLowerVolume";
        case 2:
            return "AudioMute";
        case 3:
            return "AudioRaiseVolume";
        case 4:
            return "AudioPlayPause";
        case 5:
            return "AudioStop";
        case 6:
            return "AudioPrev";
        case 7:
            return "AudioNext";
        case 8:
            return "AudioPause";
        }
    }

    return NULL;
}

keycode keycode_from_x11_keysym(uint32_t keysym) {
    if (32 <= keysym && keysym < 127) { // Ascii
        return keysym;
    } else if (0xffbe <= keysym && keysym <= 0xffd5) { // Function keys
        return 0x100 + 12 + keysym - 0xffbe;
    } else if (0xff95 <= keysym && keysym <= 0xff9f) { // Keypad keys (1)
        return 0x200 + 17 + keysym - 0xff95;
    } else if (0xffaa <= keysym && keysym <= 0xffb9) { // Keypad keys (2)
        return 0x200 + 29 + keysym - 0xffaa;
    }

    switch (keysym) {
    case 65505:
        return 0x100 + 1;
    case 65506:
        return 0x100 + 2;
    case 65507:
        return 0x100 + 3;
    case 65508:
        return 0x100 + 4;
    case 65513:
        return 0x100 + 5;
    case 65514:
        return 0x100 + 6;
    case 65515:
        return 0x100 + 7;
    case 65516:
        return 0x100 + 8;
    case 65288:
        return 0x100 + 9;
    case 65293:
        return 0x100 + 10;
    case 65289:
        return 0x100 + 11;
    case 65307:
        return 0x100 + 36;

    case 65377:
        return 0x200 + 1;
    case 65300:
        return 0x200 + 2;
    case 65299:
        return 0x200 + 3;
    case 65379:
        return 0x200 + 4;
    case 65360:
        return 0x200 + 5;
    case 65535:
        return 0x200 + 6;
    case 65367:
        return 0x200 + 7;
    case 65366:
        return 0x200 + 8;
    case 65365:
        return 0x200 + 9;
    case 65312:
        return 0x200 + 10;
    case 65383:
        return 0x200 + 11;
    case 65361:
        return 0x200 + 12;
    case 65363:
        return 0x200 + 13;
    case 65362:
        return 0x200 + 14;
    case 65364:
        return 0x200 + 15;
    case 65421:
        return 0x200 + 16;
    case 65407:
        return 0x200 + 45;
    case 65509:
        return 0x200 + 46;

    case 269025041:
        return 0x300 + 1;
    case 269025042:
        return 0x300 + 2;
    case 269025043:
        return 0x300 + 3;
    case 269025044:
        return 0x300 + 4;
    case 269025045:
        return 0x300 + 5;
    case 269025046:
        return 0x300 + 6;
    case 269025047:
        return 0x300 + 7;
    case 269025048:
        return 0x300 + 8;
    }

    return 0;
}

keycode keycode_from_win_virtual_code(uint8_t code, bool shift) {
    if (code == 0x20 || (0x30 <= code && code <= 0x39)) { // ascii
        return code;
    } else if (0x41 <= code && code <= 0x5a) { // ascii letters
        return shift ? code : code + 0x20;
    } else if (0x60 <= code && code <= 0x69) { // keypad numbers
        return 0x200 + 35 + code - 0x60;
    } else if (0x70 <= code && code <= 0x87) { // function keys
        return 0x100 + 12 + code - 0x70;
    }

    switch (code) {
    case 0x08:
        return 0x100 + 9;
    case 0x09:
        return 0x100 + 11;
    case 0x0d:
        return 0x100 + 10;
    // Shift/Ctrl/Alt without specified left/right return left
    case 0x10:
        return 0x100 + 1;
    case 0x11:
        return 0x100 + 3;
    case 0x12:
        return 0x100 + 5;
    case 0x13:
        return 0x200 + 3;
    case 0x14:
        return 0x200 + 46;
    case 0x1b:
        return 0x100 + 36;
    case 0x21:
        return 0x200 + 8;
    case 0x22:
        return 0x200 + 9;
    case 0x23:
        return 0x200 + 7;
    case 0x24:
        return 0x200 + 5;
    case 0x25:
        return 0x200 + 12;
    case 0x26:
        return 0x200 + 14;
    case 0x27:
        return 0x200 + 13;
    case 0x28:
        return 0x200 + 15;
    case 0x2c:
        return 0x200 + 1;
    case 0x2d:
        return 0x200 + 4;
    case 0x2e:
        return 0x200 + 6;
    case 0x5b:
        return 0x100 + 7;
    case 0x5c:
        return 0x100 + 8;
    case 0x6a:
        return 0x200 + 29;
    case 0x6b:
        return 0x200 + 30;
    case 0x6c:
        return 0x200 + 31;
    case 0x6d:
        return 0x200 + 32;
    case 0x6e:
        return 0x200 + 33;
    case 0x6f:
        return 0x200 + 34;
    case 0x90:
        return 0x200 + 45;
    case 0x91:
        return 0x200 + 2;
    case 0xa0:
        return 0x100 + 1;
    case 0xa1:
        return 0x100 + 2;
    case 0xa2:
        return 0x100 + 3;
    case 0xa3:
        return 0x100 + 4;
    case 0xa4:
        return 0x100 + 5;
    case 0xa5:
        return 0x100 + 6;
    case 0xad:
        return 0x300 + 2;
    case 0xae:
        return 0x300 + 1;
    case 0xaf:
        return 0x300 + 3;
    case 0xb0:
        return 0x300 + 7;
    case 0xb1:
        return 0x300 + 6;
    case 0xb2:
        return 0x300 + 5;
    case 0xb3:
        return 0x300 + 4;
    // TODO Instead of hard-coding OEM keys, somehow fetch them from windows.
    case 0xba:
        return shift ? ':' : ';';
    case 0xbb:
        return '+';
    case 0xbc:
        return ',';
    case 0xbd:
        return '-';
    case 0xbe:
        return '.';
    case 0xbf:
        return shift ? '?' : '/';
    case 0xc0:
        return shift ? '~' : '`';
    case 0xdb:
        return shift ? '{' : '[';
    case 0xdc:
        return shift ? '|' : '\\';
    case 0xdd:
        return shift ? '}' : ']';
    case 0xde:
        return shift ? '"' : '\'';
    case 0xe2:
        return shift ? '>' : '<';
    }

    return 0;
}
