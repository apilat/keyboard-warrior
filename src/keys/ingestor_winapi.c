#include "../const.h"
#include "../db.h"
#include "../log.h"
#include "../util.h"
#include "defs.h"
#include "keys.h"

#include <errno.h>
#include <pthread.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <windows.h>

struct key_ingestor {
    // shared
    atomic_bool running;
    bool on_keyboard, on_mouse;
    pthread_t worker;

    // worker
    struct key_aggregator *agg;
    HHOOK hook_handle;
};

static int worker_loop(struct key_ingestor *keys);
static LRESULT hook_procedure(int code, WPARAM wParam, LPARAM lParam);

struct key_ingestor *key_ingestor_init(struct key_aggregator *agg, bool keyboard, bool mouse) {
    debug("initializing key ingestor (winapi)");
    struct key_ingestor *keys = malloc_safe(sizeof(*keys));

    keys->agg = agg;
    keys->on_keyboard = keyboard;
    keys->on_mouse = mouse;

    atomic_store(&keys->running, true);
    if (pthread_create(&keys->worker, NULL, (void *)worker_loop, keys)) {
        error("pthread_create failed: %s\n", strerror(errno));
        goto error1;
    }

    return keys;

error1:
    free(keys);
    return NULL;
}

void key_ingestor_free(struct key_ingestor *keys) {
    debug("freeing key ingestor (winapi)");
    atomic_store(&keys->running, false);
    pthread_join(keys->worker, NULL);

    free(keys);
}

static struct key_ingestor *hook_keys;

static LRESULT hook_procedure(int nCode, WPARAM wParam, LPARAM lParam) {
    if (wParam == WM_KEYUP || wParam == WM_SYSKEYUP) {
        KBDLLHOOKSTRUCT *data = (void *)lParam;
        bool shift = GetAsyncKeyState(VK_SHIFT);
        keycode key = keycode_from_win_virtual_code(data->vkCode, shift);

        if (hook_keys->on_keyboard) {
            if (key_aggregator_record(hook_keys->agg, key)) {
                warn("failed to save keypress");
            }
        }
    }

    return CallNextHookEx(0, nCode, wParam, lParam);
}

static int worker_loop(struct key_ingestor *keys) {
    info("worker starting");

    HMODULE module_handle = GetModuleHandleA(NULL);
    if (!module_handle) {
        error("GetModuleHandleA failed: %d\n", GetLastError());
        return 1;
    }

    hook_keys = keys;
    keys->hook_handle = SetWindowsHookExA(WH_KEYBOARD_LL, hook_procedure, module_handle, 0);
    if (!keys->hook_handle) {
        error("SetWindowsHookExA failed: %d\n", GetLastError());
        return 1;
    }

    // TODO Multiplex with keys->running
    MSG msg;
    while (GetMessage(&msg, NULL, 0, 0)) {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    UnhookWindowsHookEx(keys->hook_handle);

    return 0;
}
