#ifndef WARRIOR_CONST
#define WARRIOR_CONST

// -- database
#define DB_VERSION (8)
#define DB_BUSY_TIMEOUT_MS (100)
#define MAX_TIME_OFFSET_MS (1000 * 60)
#define MOUSE_RANDOM_PIXEL_OFFSET_MAX (150)

// -- stats
#define BY_DAY_DEFAULT_COUNT (10)
#define BY_KEY_DEFAULT_COUNT (8)

#endif
