#ifndef WARRIOR_LOG
#define WARRIOR_LOG

enum log_level {
    level_trace = 1,
    level_debug = 2,
    level_info = 3,
    level_warn = 4,
    level_error = 5,
};

void set_log_level(enum log_level level);
enum log_level get_log_level();
const char* level_to_string(enum log_level level);
enum log_level string_to_level(char *s);

void log_write(enum log_level level, char* file, int line, char* fmt, ...);
#define trace(...) log_write(level_trace, __FILE__, __LINE__, __VA_ARGS__)
#define debug(...) log_write(level_debug, __FILE__, __LINE__, __VA_ARGS__)
#define info(...) log_write(level_info, __FILE__, __LINE__, __VA_ARGS__)
#define warn(...) log_write(level_warn, __FILE__, __LINE__, __VA_ARGS__)
#define error(...) log_write(level_error, __FILE__, __LINE__, __VA_ARGS__)

#endif
