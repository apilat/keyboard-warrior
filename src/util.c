#include "util.h"

#include <errno.h>
#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#ifdef __linux__
#include <sys/ioctl.h>
struct terminal_size get_terminal_size() {
    struct winsize w;
    struct terminal_size s;
    if (ioctl(STDOUT_FILENO, TIOCGWINSZ, &w) == 0) {
        s.cols = w.ws_col;
        s.rows = w.ws_row;
    } else {
        s.cols = 80;
        s.rows = 28;
    }
    return s;
}

#elif _WIN64
struct terminal_size get_terminal_size() {
    // TODO Get terminal size on windows
    warn("cannot get terminal size on win64");
    return (struct terminal_size){.cols = 80, .rows = 28};
}

#endif

void *malloc_safe(size_t size) {
    void *ptr = malloc(size);
    if (!ptr) {
        fprintf(stderr, "memory allocation failed, aborting: %s\n", strerror(errno));
        abort();
    }
    return ptr;
}

void *realloc_safe(void *ptr, size_t size) {
    ptr = realloc(ptr, size);
    if (!ptr) {
        fprintf(stderr, "memory allocation failed, aborting: %s\n", strerror(errno));
        abort();
    }
    return ptr;
}

bool buffer_push_uninit(void **buf, size_t esize, size_t *size, size_t *cap) {
    bool reallocated = false;
    if (*cap == 0) {
        *cap = 8;
        *buf = malloc_safe(esize * *cap);
        reallocated = true;
    } else if (*size == *cap) {
        *cap = *cap * 2;
        *buf = realloc_safe(*buf, esize * *cap);
        reallocated = true;
    }
    *size += 1;
    return reallocated;
}

void buffer_push(void **buf, size_t esize, size_t *size, size_t *cap, void *elem) {
    buffer_push_uninit(buf, esize, size, cap);
    memcpy(*buf + esize * (*size - 1), elem, esize);
}

#ifdef __linux__
#include <pwd.h>
char *get_default_database_file() {
    struct passwd *usr = getpwuid(getuid());
    if (!usr) {
        fprintf(stderr, "getpwuid failed: %s\n", strerror(errno));
        return NULL;
    }

    const char *file_suffix = "/.local/share/warrior.sqlite";
    size_t size = strlen(usr->pw_dir) + strlen(file_suffix) + 1;
    char *file = malloc_safe(size);
    snprintf(file, size, "%s%s", usr->pw_dir, file_suffix);
    return file;
}

#elif _WIN64
char *get_default_database_file() {
    error("cannot get default database file on windows");
    return NULL;
}

#endif

uint64_t randint() {
    uint64_t rand;
    sqlite3_randomness(sizeof(rand), &rand);
    return rand;
}

#ifdef __linux__
#include <signal.h>

int ignore_signals_pthread(void) {
    sigset_t set;
    sigemptyset(&set);
    sigaddset(&set, SIGINT);
    sigaddset(&set, SIGTERM);
    sigaddset(&set, SIGQUIT);
    sigaddset(&set, SIGPIPE);
    sigaddset(&set, SIGHUP);
    return pthread_sigmask(SIG_BLOCK, &set, NULL);
}
#endif
