#ifndef WARRIOR_DB
#define WARRIOR_DB

#include "keys/defs.h"

#include <stdbool.h>
#include <stdint.h>
#include <unistd.h>

typedef struct sqlite3 sqlite3;

#define EVT_KEY 1
#define EVT_MOUSE 2

struct keypress {
    int type;
    int64_t timestamp;
    keycode keycode;
};

struct mousepress {
    int type;
    int64_t timestamp;
    int button;
    double x, y;
};

union event {
    int type;
    struct keypress key;
    struct mousepress mouse;
};

struct database;
struct database *database_init(const char *file, bool readonly);
void database_free(struct database *db);
sqlite3 *database_get_raw(struct database *db);

int database_insert_batch(struct database *db, union event *buf, size_t len);

#endif
