#include "migration.h"
#include "const.h"
#include "db.h"
#include "log.h"
#include "util.h"

#include <sqlite3.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <unistd.h>

static int migrate_to_v1(sqlite3 *db) {
    sqlite3_stmt *stmt;

    if (sqlite3_prepare_v2(db,
                           "CREATE TABLE presses ("
                           "keysym INTEGER NOT NULL,"
                           "time INTEGER NOT NULL"
                           ")",
                           -1, &stmt, NULL) != SQLITE_OK ||
        sqlite3_step(stmt) != SQLITE_DONE || sqlite3_finalize(stmt) != SQLITE_OK) {
        fprintf(stderr, "failed to create presses table\n");
        return 1;
    }

    if (sqlite3_prepare_v2(db,
                           "CREATE TABLE keys ("
                           "keysym INTEGER PRIMARY KEY,"
                           "keyname TEXT NOT NULL"
                           ")",
                           -1, &stmt, NULL) != SQLITE_OK ||
        sqlite3_step(stmt) != SQLITE_DONE || sqlite3_finalize(stmt) != SQLITE_OK) {
        fprintf(stderr, "failed to create keys table\n");
        return 1;
    }

    return 0;
}

static int migrate_to_v2(sqlite3 *db) {
    sqlite3_stmt *stmt;

    uint32_t hash;
    sqlite3_randomness(4, &hash);
    char hostname[64] = {0};
#ifdef __linux__
    gethostname(hostname, sizeof(hostname));
#else
    // TODO Get hostname on windows
    strcpy(hostname, "unknown");
#endif
    if (hostname[sizeof(hostname) - 1] != '\0') {
        fprintf(stderr, "hostname too long\n");
        return 1;
    }

    if (sqlite3_prepare_v2(db,
                           "CREATE TABLE new_presses ("
                           "keysym INTEGER NOT NULL,"
                           "time INTEGER NOT NULL,"
                           "hash INTEGER NOT NULL"
                           ")",
                           -1, &stmt, NULL) != SQLITE_OK ||
        sqlite3_step(stmt) != SQLITE_DONE || sqlite3_finalize(stmt) != SQLITE_OK) {
        fprintf(stderr, "failed to create new presses table\n");
        return 1;
    }
    if (sqlite3_prepare_v2(db,
                           "INSERT INTO new_presses"
                           " SELECT keysym, time, ? FROM presses",
                           -1, &stmt, NULL) != SQLITE_OK ||
        sqlite3_bind_int64(stmt, 1, hash) != SQLITE_OK || sqlite3_step(stmt) != SQLITE_DONE ||
        sqlite3_finalize(stmt) != SQLITE_OK) {
        fprintf(stderr, "failed to transfer presses table data\n");
        return 1;
    }
    if (sqlite3_prepare_v2(db, "DROP TABLE presses", -1, &stmt, NULL) != SQLITE_OK ||
        sqlite3_step(stmt) != SQLITE_DONE || sqlite3_finalize(stmt) != SQLITE_OK) {
        fprintf(stderr, "failed to drop old presses table\n");
        return 1;
    }
    if (sqlite3_prepare_v2(db, "ALTER TABLE new_presses RENAME TO presses", -1, &stmt, NULL) !=
            SQLITE_OK ||
        sqlite3_step(stmt) != SQLITE_DONE || sqlite3_finalize(stmt) != SQLITE_OK) {
        fprintf(stderr, "failed to drop old presses table\n");
        return 1;
    }

    if (sqlite3_prepare_v2(db, "CREATE INDEX by_keysym ON presses (keysym)", -1, &stmt, NULL) !=
            SQLITE_OK ||
        sqlite3_step(stmt) != SQLITE_DONE || sqlite3_finalize(stmt) != SQLITE_OK) {
        fprintf(stderr, "failed to create index on keysym\n");
        return 1;
    }

    if (sqlite3_prepare_v2(db,
                           "CREATE TABLE sync ("
                           "hash INTEGER PRIMARY KEY,"
                           "name TEXT,"
                           "last_sync_time INTEGER NOT NULL"
                           ")",
                           -1, &stmt, NULL) != SQLITE_OK ||
        sqlite3_step(stmt) != SQLITE_DONE || sqlite3_finalize(stmt) != SQLITE_OK) {
        fprintf(stderr, "failed to create sync table\n");
        return 1;
    }
    if (sqlite3_prepare_v2(db, "INSERT INTO sync VALUES (?, ?, -1)", -1, &stmt, NULL) !=
            SQLITE_OK ||
        sqlite3_bind_int64(stmt, 1, hash) != SQLITE_OK ||
        sqlite3_bind_text(stmt, 2, hostname, -1, SQLITE_STATIC) != SQLITE_OK ||
        sqlite3_step(stmt) != SQLITE_DONE || sqlite3_finalize(stmt) != SQLITE_OK) {
        fprintf(stderr, "failed to insert self into sync table\n");
        return 1;
    }

    return 0;
}

static int migrate_to_v3(sqlite3 *db) {
    if (sqlite3_exec(db, "DROP INDEX by_keysym", NULL, NULL, NULL) != SQLITE_OK) {
        fprintf(stderr, "failed to remove index on keysym\n");
        return 1;
    }

    if (sqlite3_exec(db, "CREATE INDEX full ON presses(hash, keysym, time)", NULL, NULL, NULL) !=
        SQLITE_OK) {
        fprintf(stderr, "failed to create full index\n");
        return 1;
    }

    // Run analyze here to convince the query analyzer to use skip-scans on
    // full for most queries.
    if (sqlite3_exec(db, "ANALYZE", NULL, NULL, NULL) != SQLITE_OK) {
        fprintf(stderr, "failed to analyze database\n");
        return 1;
    }

    return 0;
}

static int migrate_to_v4(sqlite3 *db) {
    // Recreate presses with new schema
    if (sqlite3_exec(db,
                     "CREATE TABLE new_presses ("
                     "keysym INTEGER NOT NULL,"
                     "time INTEGER NOT NULL,"
                     "peer_id INTEGER NOT NULL,"
                     "uid INTEGER PRIMARY KEY"
                     ")",
                     NULL, NULL, NULL) != SQLITE_OK) {
        fprintf(stderr, "failed to create new_presses\n");
        return 1;
    }
    if (sqlite3_exec(db,
                     "INSERT INTO new_presses"
                     " SELECT keysym, time, hash, random()"
                     " FROM presses",
                     NULL, NULL, NULL) != SQLITE_OK) {
        fprintf(stderr, "failed to transfer data to new_presses\n");
        return 1;
    }
    if (sqlite3_exec(db, "DROP TABLE presses", NULL, NULL, NULL) != SQLITE_OK) {
        fprintf(stderr, "failed to drop presses\n");
        return 1;
    }
    if (sqlite3_exec(db, "ALTER TABLE new_presses RENAME TO presses", NULL, NULL, NULL) !=
        SQLITE_OK) {
        fprintf(stderr, "failed to rename new_presses\n");
        return 1;
    }
    if (sqlite3_exec(db, "CREATE INDEX search ON presses(peer_id, keysym, time)", NULL, NULL,
                     NULL) != SQLITE_OK) {
        fprintf(stderr, "failed to create search index\n");
        return 1;
    }

    // Recreate sync with new schema
    if (sqlite3_exec(db,
                     "CREATE TABLE peers ("
                     "peer_id INTEGER PRIMARY KEY,"
                     "name TEXT,"
                     "is_self INTEGER"
                     ")",
                     NULL, NULL, NULL) != SQLITE_OK) {
        fprintf(stderr, "failed to create peers\n");
        return 1;
    }
    // Since we didn't yet have facilities to add peers, any peer must be us.
    if (sqlite3_exec(db,
                     "INSERT INTO peers"
                     " SELECT hash, name, 1"
                     " FROM sync",
                     NULL, NULL, NULL) != SQLITE_OK) {
        fprintf(stderr, "failed to transfer data to peers\n");
        return 1;
    }
    if (sqlite3_exec(db, "DROP TABLE sync", NULL, NULL, NULL) != SQLITE_OK) {
        fprintf(stderr, "failed to drop presses\n");
        return 1;
    }

    if (sqlite3_exec(db, "ANALYZE", NULL, NULL, NULL) != SQLITE_OK) {
        fprintf(stderr, "failed to run analyze\n");
        return 1;
    }

    return 0;
}

static int migrate_to_v5(sqlite3 *db) {
    // Create keycode mapping table
    if (sqlite3_exec(db,
                     "CREATE TEMP TABLE keys_map ("
                     "keycode INTEGER NOT NULL,"
                     "keysym INTEGER PRIMARY KEY"
                     ")",
                     NULL, NULL, NULL) != SQLITE_OK) {
        error("create keys_map failed: %s", sqlite3_errmsg(db));
        return 1;
    }

    {
        sqlite3_stmt *select, *insert;
        if (sqlite3_prepare_v2(db, "SELECT keysym FROM keys", -1, &select, NULL) != SQLITE_OK) {
            error("sqlite3_prepare(select) failed: %s", sqlite3_errmsg(db));
            return 1;
        }
        if (sqlite3_prepare_v2(db, "INSERT INTO keys_map VALUES (?, ?)", -1, &insert, NULL) !=
            SQLITE_OK) {
            error("sqlite3_prepare(insert) failed: %s", sqlite3_errmsg(db));
            return 1;
        }

        int ret;
        while ((ret = sqlite3_step(select)) == SQLITE_ROW) {
            uint32_t keysym = sqlite3_column_int64(select, 0);
            keycode code = keycode_from_x11_keysym(keysym);

            if (sqlite3_bind_int64(insert, 1, code) != SQLITE_OK ||
                sqlite3_bind_int64(insert, 2, keysym) != SQLITE_OK) {
                error("sqlite3_bind(insert) failed: %s", sqlite3_errmsg(db));
                return 1;
            }
            if (sqlite3_step(insert) != SQLITE_DONE) {
                error("sqlite3_step(insert) failed: %s", sqlite3_errmsg(db));
                return 1;
            }
            if (sqlite3_reset(insert) != SQLITE_OK) {
                error("sqlite3_reset(insert) failed: %s", sqlite3_errmsg(db));
                return 1;
            }
        }

        if (ret != SQLITE_DONE) {
            error("sqlite3_step(select) failed: %s", sqlite3_errstr(ret));
            return 1;
        }

        if (sqlite3_finalize(select) != SQLITE_OK) {
            error("sqlite3_finalize(select) failed: %s", sqlite3_errmsg(db));
            return 1;
        }
        if (sqlite3_finalize(insert) != SQLITE_OK) {
            error("sqlite3_finalize(insert) failed: %s", sqlite3_errmsg(db));
            return 1;
        }
    }

    // Drop keysym list
    if (sqlite3_exec(db, "DROP TABLE keys", NULL, NULL, NULL) != SQLITE_OK) {
        error("drop keys failed: %s", sqlite3_errmsg(db));
        return 1;
    }

    // Recreate presses with new schema
    if (sqlite3_exec(db,
                     "CREATE TABLE presses_new ("
                     "uid INTEGER PRIMARY KEY,"
                     "peer_id INTEGER NOT NULL,"
                     "time INTEGER NOT NULL,"
                     "keycode INTEGER NOT NULL"
                     ")",
                     NULL, NULL, NULL) != SQLITE_OK) {
        error("create presses_new failed: %s", sqlite3_errmsg(db));
        return 1;
    }
    if (sqlite3_exec(db,
                     "INSERT INTO presses_new"
                     " SELECT uid, peer_id, time, keys_map.keycode"
                     " FROM presses INNER JOIN keys_map ON keys_map.keysym = presses.keysym",
                     NULL, NULL, NULL) != SQLITE_OK) {
        error("insert into presses_new failed: %s", sqlite3_errmsg(db));
        return 1;
    }
    if (sqlite3_exec(db, "DROP TABLE presses", NULL, NULL, NULL) != SQLITE_OK) {
        error("drop presses failed: %s", sqlite3_errmsg(db));
        return 1;
    }
    if (sqlite3_exec(db, "ALTER TABLE presses_new RENAME TO presses", NULL, NULL, NULL) !=
        SQLITE_OK) {
        error("rename presses_presses failed: %s", sqlite3_errmsg(db));
        return 1;
    }

    // Drop temporary table
    if (sqlite3_exec(db, "DROP TABLE keys_map", NULL, NULL, NULL) != SQLITE_OK) {
        error("drop keys_map failed: %s", sqlite3_errmsg(db));
        return 1;
    }

    return 0;
}

static int migrate_to_v6(sqlite3 *db) {
    if (sqlite3_exec(db,
                     "CREATE TABLE presses_new ("
                     "peer_id INTEGER NOT NULL,"
                     "counter INTEGER NOT NULL,"
                     "timestamp INTEGER NOT NULL,"
                     "keycode INTEGER NOT NULL,"
                     "PRIMARY KEY (peer_id, counter)"
                     ") WITHOUT ROWID",
                     NULL, NULL, NULL) != SQLITE_OK) {
        error("create presses_new failed: %s", sqlite3_errmsg(db));
        return 1;
    }

    // Previous versions of the database could have accidentally inserted keys in
    // the correct order since buffering was bugged.
    // Randomly order the rows here to get rid of the information.
    if (sqlite3_exec(db,
                     "INSERT INTO presses_new"
                     " SELECT peer_id, row_number() OVER (PARTITION BY peer_id ORDER BY RANDOM()), "
                     "time * 1000 + RANDOM() % 1000, keycode"
                     " FROM presses",
                     NULL, NULL, NULL) != SQLITE_OK) {
        error("insert into presses_new failed: %s", sqlite3_errmsg(db));
        return 1;
    }

    if (sqlite3_exec(db, "DROP TABLE presses", NULL, NULL, NULL) != SQLITE_OK) {
        error("drop presses failed: %s", sqlite3_errmsg(db));
        return 1;
    }

    if (sqlite3_exec(db, "ALTER TABLE presses_new RENAME TO presses", NULL, NULL, NULL) !=
        SQLITE_OK) {
        error("rename presses_new failed: %s", sqlite3_errmsg(db));
        return 1;
    }

    if (sqlite3_exec(db,
                     "CREATE TABLE peers_new ("
                     "peer_id INTEGER NOT NULL,"
                     "hostname TEXT,"
                     "is_self INTEGER NOT NULL,"
                     "record_count INTEGER NOT NULL"
                     ")",
                     NULL, NULL, NULL) != SQLITE_OK) {
        error("create peers_new failed: %s", sqlite3_errmsg(db));
        return 1;
    }

    if (sqlite3_exec(db,
                     "INSERT INTO peers_new"
                     " SELECT peer_id, name, is_self,"
                     " (SELECT COUNT(*) FROM presses WHERE presses.peer_id = peers.peer_id)"
                     " FROM peers",
                     NULL, NULL, NULL) != SQLITE_OK) {
        error("insert into peers_new failed: %s", sqlite3_errmsg(db));
        return 1;
    }

    if (sqlite3_exec(db, "DROP TABLE peers", NULL, NULL, NULL) != SQLITE_OK) {
        error("drop peers failed: %s", sqlite3_errmsg(db));
        return 1;
    }

    if (sqlite3_exec(db, "ALTER TABLE peers_new RENAME TO peers", NULL, NULL, NULL) != SQLITE_OK) {
        error("rename peers_new failed: %s", sqlite3_errmsg(db));
        return 1;
    }

    if (sqlite3_exec(db, "CREATE INDEX presses_query ON presses (peer_id, timestamp, keycode)",
                     NULL, NULL, NULL) != SQLITE_OK) {
        error("rename peers_new failed: %s", sqlite3_errmsg(db));
        return 1;
    }

    if (sqlite3_exec(db, "ANALYZE", NULL, NULL, NULL) != SQLITE_OK) {
        error("analyze failed: %s", sqlite3_errmsg(db));
        return 1;
    }

    return 0;
}

static const char *commands_v7[] = {
    "CREATE TABLE presses_new ("
    "timestamp INTEGER NOT NULL,"
    "keycode INTEGER NOT NULL);",
    "INSERT INTO presses_new SELECT timestamp, keycode"
    " FROM presses JOIN peers ON presses.peer_id = peers.peer_id"
    " WHERE is_self = 1",
    "DROP TABLE presses;", "ALTER TABLE presses_new RENAME TO presses;", "DROP TABLE peers;"};

static const char *commands_v8[] = {"CREATE TABLE mouse_presses ("
                                    "timestamp INTEGER NOT NULL,"
                                    "button INTEGER NOT NULL,"
                                    "x REAL NOT NULL,"
                                    "y REAL NOT NULL);"};

static int migrate_simple(sqlite3 *db, int version, const char **commands, size_t command_size) {
    for (size_t i = 0; i < command_size; i++) {
        char *err;
        if (sqlite3_exec(db, commands[i], NULL, NULL, &err) != SQLITE_OK) {
            error("migration to v%d failed step %d (\"%s\"): %s", version, i, commands[i], err);
            sqlite3_free(err);
            return 1;
        }
    }
    return 0;
}

int migrate_version(sqlite3 *db, bool readonly) {
    sqlite3_stmt *stmt;

    if (sqlite3_prepare_v2(db, "BEGIN", -1, &stmt, NULL) != SQLITE_OK ||
        sqlite3_step(stmt) != SQLITE_DONE || sqlite3_finalize(stmt) != SQLITE_OK) {
        error("sqlite3 transaction begin failed: %s", sqlite3_errmsg(db));
        return 1;
    }

    if (sqlite3_prepare_v2(db, "PRAGMA user_version", -1, &stmt, NULL) != SQLITE_OK) {
        error("sqlite3_prepare(user_version) failed: %s", sqlite3_errmsg(db));
        goto rollback;
    }
    if (sqlite3_step(stmt) != SQLITE_ROW) {
        error("sqlite3_step(user_version) failed: %s", sqlite3_errmsg(db));
        goto rollback;
    }
    int user_version = sqlite3_column_int(stmt, 0);
    if (sqlite3_step(stmt) != SQLITE_DONE) {
        error("sqlite3_step(user_version) failed: %s", sqlite3_errmsg(db));
        goto rollback;
    }
    if (sqlite3_finalize(stmt) != SQLITE_OK) {
        error("sqlite3_finalize(user_version) failed: %s", sqlite3_errmsg(db));
        goto rollback;
    }

    if (user_version != DB_VERSION) {
        if (readonly) {
            error("database needs migration but we are in read-only mode");
            goto rollback;
        }

        info("migrating database to newest version %d", DB_VERSION);

        switch (user_version) {
        case 0:
            if (migrate_to_v1(db)) {
                goto rollback;
            }
            __attribute__((fallthrough));
        case 1:
            if (migrate_to_v2(db)) {
                goto rollback;
            }
            __attribute__((fallthrough));
        case 2:
            if (migrate_to_v3(db)) {
                goto rollback;
            }
            __attribute__((fallthrough));
        case 3:
            if (migrate_to_v4(db)) {
                goto rollback;
            }
            __attribute__((fallthrough));
        case 4:
            if (migrate_to_v5(db)) {
                goto rollback;
            }
            __attribute__((fallthrough));
        case 5:
            if (migrate_to_v6(db)) {
                goto rollback;
            }
            __attribute__((fallthrough));
        case 6:
            if (migrate_simple(db, 7, commands_v7, ARRAY_SIZE(commands_v7))) {
                goto rollback;
            }
            __attribute__((fallthrough));
        case 7:
            if (migrate_simple(db, 8, commands_v8, ARRAY_SIZE(commands_v8))) {
                goto rollback;
            }
            break;
        default:
            error("unrecognized database version %d, refusing to run to avoid corruption",
                  user_version);
            goto rollback;
        }

        char update_ver[32];
        if (snprintf(update_ver, sizeof(update_ver), "PRAGMA user_version = %d", DB_VERSION) >=
            (int)sizeof(update_ver)) {
            error("failed to create query for user_version update: this should be impossible");
            goto rollback;
        }
        if (sqlite3_prepare_v2(db, update_ver, -1, &stmt, NULL) != SQLITE_OK ||
            sqlite3_step(stmt) != SQLITE_DONE || sqlite3_finalize(stmt) != SQLITE_OK) {
            error("sqlite3(PRAGMA user_version) failed: %s", sqlite3_errmsg(db));
            goto rollback;
        }
    }

    if (sqlite3_prepare_v2(db, "COMMIT", -1, &stmt, NULL) != SQLITE_OK ||
        sqlite3_step(stmt) != SQLITE_DONE || sqlite3_finalize(stmt) != SQLITE_OK) {
        error("sqlite3 transaction commit failed: %s", sqlite3_errmsg(db));
        goto rollback;
    }

    return 0;

rollback:
    if (sqlite3_prepare_v2(db, "ROLLBACK", -1, &stmt, NULL) != SQLITE_OK ||
        sqlite3_step(stmt) != SQLITE_DONE || sqlite3_finalize(stmt) != SQLITE_OK) {
        error("sqlite3 transaction rollback failed: %s", sqlite3_errmsg(db));
    }
    return 1;
}
