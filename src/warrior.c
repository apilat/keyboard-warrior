#include "db.h"
#include "keys/keys.h"
#include "log.h"
#include "stats/stats.h"
#include "util.h"

#include <errno.h>
#include <getopt.h>
#include <signal.h>
#include <sqlite3.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

static atomic_bool running;
char *exe_name;

#ifdef __linux__
static void signal_handler(int _signo, siginfo_t *_info, void *_ctx) {
    (void)_signo;
    (void)_info;
    (void)_ctx;
    atomic_store(&running, false);
}

static void signal_ignore(int _signo, siginfo_t *_info, void *_ctx) {
    (void)_signo;
    (void)_info;
    (void)_ctx;
}
#elif _WIN32
#endif

static void stats_usage() {
    char *db_file = get_default_database_file();
    bool alloc = db_file != NULL;
    if (!db_file) {
        db_file = "<unknown>";
    }

    fprintf(stderr,
            "Usage: %s stats [options]\n"
            " -h,       --help         display this help page\n"
            " -v,       --verbose      display more statistics\n"
            " -D file,  --use-db file  read specified database file [default=%s]\n",
            exe_name, db_file);

    if (alloc) {
        free(db_file);
    }
}

static int main_stats(int argc, char *argv[]) {
    const char *opts = "hvD:";
    const struct option long_opts[] = {{"help", no_argument, NULL, 'h'},
                                       {"verbose", no_argument, NULL, 'v'},
                                       {"use-db", required_argument, NULL, 'D'},
                                       {0, 0, 0, 0}};

    int level = 1;
    char *db_file = NULL;
    struct database *db = NULL;

    int c;
    while ((c = getopt_long(argc, argv, opts, long_opts, NULL)) != -1) {
        switch (c) {
        case 0:
            fprintf(stderr, "unexpected argument %s\n", optarg);
            goto user_error;
        case 'h':
            goto user_error;
        case 'v':
            level += 1;
            break;
        case 'D':
            if (db_file) {
                fprintf(stderr, "duplicate database specification\n");
                goto user_error;
            }
            db_file = strdup(optarg);
            break;
        default:
            goto user_error;
        }
    }

    if (optind != argc) {
        fprintf(stderr, "unexpected argument %s\n", argv[optind]);
        goto user_error;
    }

    if (!db_file) {
        db_file = get_default_database_file();
        if (!db_file) {
            fprintf(stderr, "failed to get default database location\n");
            goto error;
        }
    }

    db = database_init(db_file, true);
    if (!db) {
        fprintf(stderr, "failed to load database\n");
        goto error;
    }

    if (stats_print_overview(db, level)) {
        fprintf(stderr, "failed to print stats\n");
        goto error;
    }

    int ret = 0;
free:
    if (db) {
        database_free(db);
    }
    free(db_file);
    return ret;
error:
    ret = 1;
    goto free;
user_error:
    stats_usage();
    ret = 1;
    goto free;
}

static void daemon_usage(void) {
    char *db_file = get_default_database_file();
    bool alloc = db_file != NULL;
    if (!db_file) {
        db_file = "<unknown>";
    }

    fprintf(
        stderr,
        "Usage: %s daemon [options]\n"
        " -h,                --help                        display this help page\n"
        " -D [file],         --use-db [file]               write to database [default=%s]\n"
        " -K,                --use-keys                    collect keyboard presses "
        "[default=false]\n"
        " -M,                --use-mouse                   collect mouse clicks [default=false]\n"
        " -B size,           --buffer-size size            set key aggregator buffer size "
        "[default=256]\n",
        exe_name, db_file);

    if (alloc) {
        free(db_file);
    }
}

static int main_daemon(int argc, char *argv[]) {
    const char *opts = "hD::KMC:S::P:B:";
    const struct option long_opts[] = {{"help", no_argument, NULL, 'h'},
                                       {"use-db", optional_argument, NULL, 'D'},
                                       {"use-keys", no_argument, NULL, 'K'},
                                       {"use-mouse", no_argument, NULL, 'M'},
                                       {"buffer-size", required_argument, NULL, 'B'},
                                       {"buffer-cap", required_argument, NULL, 'B'},
                                       {0, 0, 0, 0}};

    bool use_keys = false, use_mouse = false;
    char *db_file = NULL;
    size_t buffer_cap = 256;

    struct database *db = NULL;
    struct key_ingestor *ingestor = NULL;
    struct key_aggregator *aggregator = NULL;

    int c;
    optind = 0;
    while ((c = getopt_long(argc, argv, opts, long_opts, NULL)) != -1) {
        // Greedily handle optional arguments.
        // For some reason this getopt chooses not to do this.
        char *opt;
        if (c == 'D' && optarg == NULL && argv[optind] != NULL && argv[optind][0] != '-') {
            opt = argv[optind];
            optind += 1;
        } else {
            opt = optarg;
        }

        switch (c) {
        case 0:
            fprintf(stderr, "unexpected argument %s\n", optarg);
            goto user_error;

        case 'h':
            goto user_error;

        case 'D':
            if (db_file) {
                fprintf(stderr, "duplicate database specification\n");
                goto user_error;
            }

            if (opt) {
                db_file = strdup(opt);
            } else {
                db_file = get_default_database_file();
                if (!db_file) {
                    fprintf(stderr, "failed to get default database location\n");
                    goto error;
                }
            }
            break;

        case 'K':
            use_keys = true;
            break;

        case 'M':
            use_mouse = true;
            break;

        case 'B': {
            char *end;
            buffer_cap = strtol(optarg, &end, 10);
            if (*end != '\0') {
                fprintf(stderr, "failed to parse buffer capacity %s\n", optarg);
                goto user_error;
            }
            break;
        }

        default:
            goto user_error;
        }
    }

    if (optind != argc) {
        fprintf(stderr, "unexpected argument %s\n", argv[optind]);
        goto user_error;
    }

    if (db_file) {
        info("opening database %s", db_file);
        db = database_init(db_file, false);
        if (!db) {
            fprintf(stderr, "failed to connect to database\n");
            goto error;
        }
    }

    if (use_keys || use_mouse) {
        info("starting key scanner");
        aggregator = key_aggregator_init(db, buffer_cap);
        if (!aggregator) {
            error("failed to initialize key aggregator");
            goto error;
        }
        ingestor = key_ingestor_init(aggregator, use_keys, use_mouse);
        if (!ingestor) {
            error("failed to initialize key ingestor");
            goto error;
        }
    }

    info("starting daemon loop");
    atomic_store(&running, true);
    while (atomic_load(&running)) {
        // sleep while background threads do work
        // a signal will interrupt this and allow us to exit
        sleep(86400);
    }
    info("starting daemon exit");

    int ret = 0;
free:
    if (ingestor) {
        key_ingestor_free(ingestor);
    }
    if (aggregator) {
        key_aggregator_free(aggregator);
    }
    if (db) {
        database_free(db);
    }

    free(db_file);

    return ret;

error:
    ret = 1;
    goto free;

user_error:
    daemon_usage();
    ret = 1;
    goto free;
}

static enum log_level get_log_level_from_env() {
    char *s = getenv("LOG_LEVEL");
    if (!s) {
        return 0;
    }

    enum log_level l = string_to_level(s);
    if (!l) {
        warn("cannot parse log level %s, resetting to default", s);
    }
    return l;
}

static void setup_logging() {
    enum log_level level = get_log_level_from_env();
    if (level == 0) {
        level = level_warn;
    }
    set_log_level(level);
    debug("setting log level to %s", level_to_string(level));
}

static void sqlite_error_callback(void *user_data, int code, const char *msg) {
    (void)user_data;
    warn("sqlite error occurred: %s (%d)", msg, code);
}

static int setup_sqlite() {
    int ret;
    if ((ret = sqlite3_config(SQLITE_CONFIG_LOG, sqlite_error_callback, NULL)) != SQLITE_OK) {
        warn("sqlite3_config(LOG) failed: %s", sqlite3_errstr(ret));
    }
    if ((ret = sqlite3_initialize()) != SQLITE_OK) {
        error("sqlite3_initialize failed: %s", sqlite3_errstr(ret));
        return 1;
    }
    return 0;
}

static void main_usage(void) {
    fprintf(stderr,
            "Usage: %s [daemon | stats]\n"
            " help    show this help page\n"
            " daemon  run background daemon\n"
            " stats   query database for statistics\n",
            exe_name);
}

int main(int argc, char *argv[]) {
    exe_name = argc >= 1 ? argv[0] : "warrior";

    setup_logging();
    info("starting");

    if (setup_sqlite()) {
        error("failed to setup sqlite library");
        return 1;
    }

#ifdef __linux__
    debug("setting up signal handlers");
    struct sigaction act = {0};
    act.sa_sigaction = &signal_handler;
    if (sigaction(SIGINT, &act, NULL) || sigaction(SIGTERM, &act, NULL) ||
        sigaction(SIGQUIT, &act, NULL)) {
        error("sigaction failed: %s", strerror(errno));
        return 1;
    }
    act.sa_sigaction = &signal_ignore;
    if (sigaction(SIGPIPE, &act, NULL) || sigaction(SIGHUP, &act, NULL)) {
        error("sigaction failed: %s", strerror(errno));
        return 1;
    }
#elif _WIN32
    // TODO Handle Ctrl-C on Windows
    warn("not setting signal handlers: not yet supported on Windows");
#endif

    debug("initializing rng");
    srand(time(NULL));

    int ret = 1;

    if (argc >= 2) {
        char *cmd = argv[1];
        argv[1] = argv[0];
        if (strcmp(cmd, "stats") == 0) {
            ret = main_stats(argc - 1, argv + 1);
        } else if (strcmp(cmd, "daemon") == 0) {
            ret = main_daemon(argc - 1, argv + 1);
        } else if (strcmp(cmd, "help") == 0 || strcmp(cmd, "--help") == 0 ||
                   strcmp(cmd, "-h") == 0) {
            main_usage();
        } else {
            fprintf(stderr, "unrecognized subcommand %s\n", cmd);
            main_usage();
        }
    } else {
        main_usage();
    }

    info("exiting");
    return ret;
}
