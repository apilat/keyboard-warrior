#include "db.h"
#include "const.h"
#include "log.h"
#include "migration.h"
#include "util.h"

#include <sqlite3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

// TODO Figure out if we need a global lock on the database
struct database {
    bool readonly;
    sqlite3 *db;
    sqlite3_stmt *insert_press_stmt, *insert_mouse_press_stmt, *begin_stmt, *rollback_stmt, *commit_stmt;
};

static void finalize_prepared_statements(struct database *db) {
    if (sqlite3_finalize(db->insert_press_stmt) != SQLITE_OK) {
        warn("sqlite3_finalize(insert_press) failed: %s", sqlite3_errmsg(db->db));
    }
    if (sqlite3_finalize(db->insert_mouse_press_stmt) != SQLITE_OK) {
        warn("sqlite3_finalize(insert_mouse_press) failed: %s", sqlite3_errmsg(db->db));
    }
    if (sqlite3_finalize(db->begin_stmt) != SQLITE_OK) {
        warn("sqlite3_finalize(begin) failed: %s", sqlite3_errmsg(db->db));
    }
    if (sqlite3_finalize(db->rollback_stmt) != SQLITE_OK) {
        warn("sqlite3_finalize(begin) failed: %s", sqlite3_errmsg(db->db));
    }
    if (sqlite3_finalize(db->commit_stmt) != SQLITE_OK) {
        warn("sqlite3_finalize(commit) failed: %s", sqlite3_errmsg(db->db));
    }
}

struct database *database_init(const char *file, bool readonly) {
    debug("initializing database file=%s readonly=%d", file, readonly);
    struct database *db = malloc_safe(sizeof(*db));
    db->readonly = readonly;
    db->begin_stmt = NULL;
    db->commit_stmt = NULL;
    db->rollback_stmt = NULL;
    db->insert_press_stmt = NULL;
    db->insert_mouse_press_stmt = NULL;

    int flags = SQLITE_OPEN_FULLMUTEX;
    if (readonly) {
        flags |= SQLITE_OPEN_READONLY;
    } else {
        flags |= SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE;
    }

    int ret;
    if ((ret = sqlite3_open_v2(file, &db->db, flags, NULL)) != SQLITE_OK) {
        error("sqlite3_open failed: %s", sqlite3_errstr(ret));
        goto error;
    }

    if ((ret = sqlite3_extended_result_codes(db->db, 1)) != SQLITE_OK) {
        warn("sqlite3_extended_result_codes failed: %s", sqlite3_errstr(ret));
    }

    if ((ret = sqlite3_busy_timeout(db->db, DB_BUSY_TIMEOUT_MS)) != SQLITE_OK) {
        warn("sqlite3_busy_timeout failed: %s", sqlite3_errstr(ret));
    }

    sqlite3_stmt *stmt;
    if (sqlite3_prepare_v2(db->db, "PRAGMA journal_mode=WAL", -1, &stmt, NULL) != SQLITE_OK) {
        warn("sqlite3_prepare(journal_mode) failed: %s", sqlite3_errmsg(db->db));
    } else {
        if (sqlite3_step(stmt) != SQLITE_ROW ||
            strcmp((const char *)sqlite3_column_text(stmt, 0), "wal") != 0 ||
            sqlite3_step(stmt) != SQLITE_DONE) {
            warn("pragma(journal_mode)=wal failed: %s", sqlite3_errmsg(db->db));
        }
        if (sqlite3_finalize(stmt) != SQLITE_OK) {
            warn("sqlite3_finalize(journal_mode) failed: %s", sqlite3_errmsg(db->db));
        }
    }

    if (migrate_version(db->db, readonly)) {
        error("database version migration failed");
        goto error;
    }

    if ((ret = sqlite3_prepare_v2(db->db, "BEGIN", -1, &db->begin_stmt, NULL)) != SQLITE_OK) {
        error("sqlite3_prepare(begin) failed: %s", sqlite3_errstr(ret));
        goto error;
    }
    if ((ret = sqlite3_prepare_v2(db->db, "ROLLBACK", -1, &db->rollback_stmt, NULL)) != SQLITE_OK) {
        error("sqlite3_prepare(rollback) failed: %s", sqlite3_errstr(ret));
        goto error;
    }
    if ((ret = sqlite3_prepare_v2(db->db, "INSERT INTO presses (timestamp, keycode) VALUES (?, ?)",
                                  -1, &db->insert_press_stmt, NULL)) != SQLITE_OK) {
        error("sqlite3_prepare(insert_press) failed: %s", sqlite3_errstr(ret));
        goto error;
    }
    if ((ret = sqlite3_prepare_v2(db->db, "COMMIT", -1, &db->commit_stmt, NULL)) != SQLITE_OK) {
        error("sqlite3_prepare(commit) failed: %s", sqlite3_errstr(ret));
        goto error;
    }
    if ((ret = sqlite3_prepare_v2(db->db, "INSERT INTO mouse_presses (timestamp, button, x, y) VALUES (?, ?, ?, ?)", -1, &db->insert_mouse_press_stmt, NULL)) != SQLITE_OK) {
        error("sqlite3_prepare(insert_mouse_press) failed: %s", sqlite3_errstr(ret));
        goto error;
    }

    return db;
error:
    finalize_prepared_statements(db);
    free(db);
    return NULL;
}

void database_free(struct database *db) {
    debug("freeing database");

    sqlite3_stmt *stmt;

    if (!db->readonly) {
        if (sqlite3_prepare_v2(db->db, "PRAGMA analysis_limit=1000", -1, &stmt, NULL) !=
                SQLITE_OK ||
            sqlite3_step(stmt) != SQLITE_ROW || sqlite3_step(stmt) != SQLITE_DONE ||
            sqlite3_finalize(stmt) != SQLITE_OK) {
            warn("pragma analysis_limit failed: %s", sqlite3_errmsg(db->db));
        } else if (sqlite3_prepare_v2(db->db, "ANALYZE", -1, &stmt, NULL) != SQLITE_OK ||
                   sqlite3_step(stmt) != SQLITE_DONE || sqlite3_finalize(stmt) != SQLITE_OK) {
            warn("database analysis failed: %s", sqlite3_errmsg(db->db));
        }
    }

    finalize_prepared_statements(db);

    if (sqlite3_close(db->db) != SQLITE_OK) {
        error("sqlite3_close failed: %s", sqlite3_errmsg(db->db));
    }

    free(db);
}

static int insert_single_key(struct database *db, union event evt) {
    trace("inserting press keycode=%u,keyname=%s,timestamp=%ld", evt.key.keycode,
          keycode_to_string(evt.key.keycode), evt.key.timestamp);

    if (sqlite3_bind_int64(db->insert_press_stmt, 1, evt.key.timestamp) != SQLITE_OK ||
        sqlite3_bind_int64(db->insert_press_stmt, 2, evt.key.keycode) != SQLITE_OK) {
        error("sqlite3_bind(insert_press) failed: %s", sqlite3_errmsg(db->db));
        return 1;
    }

    if (sqlite3_step(db->insert_press_stmt) != SQLITE_DONE) {
        error("sqlite3_step(insert_press) failed: %s", sqlite3_errmsg(db->db));
        if (sqlite3_reset(db->insert_press_stmt) != SQLITE_OK) {
            error("sqlite3_reset(insert_press) failed: %s", sqlite3_errmsg(db->db));
        }
        return 1;
    }

    if (sqlite3_reset(db->insert_press_stmt) != SQLITE_OK) {
        error("sqlite3_reset(insert_press) failed: %s", sqlite3_errmsg(db->db));
        return 1;
    }

    return 0;
}

static int insert_single_mouse(struct database *db, union event evt) {
    trace("inserting mouse press timestamp=%ld button=%d x=%lf y=%lf", evt.mouse.timestamp,
          evt.mouse.button, evt.mouse.x, evt.mouse.y);

    if (sqlite3_bind_int64(db->insert_mouse_press_stmt, 1, evt.mouse.timestamp) != SQLITE_OK ||
        sqlite3_bind_int64(db->insert_mouse_press_stmt, 2, evt.mouse.button) != SQLITE_OK ||
        sqlite3_bind_double(db->insert_mouse_press_stmt, 3, evt.mouse.x) != SQLITE_OK ||
        sqlite3_bind_double(db->insert_mouse_press_stmt, 4, evt.mouse.y) != SQLITE_OK
        ) {
        error("sqlite3_bind(insert_mouse_press) failed: %s", sqlite3_errmsg(db->db));
        return 1;
    }

    if (sqlite3_step(db->insert_mouse_press_stmt) != SQLITE_DONE) {
        error("sqlite3_step(insert_mouse_press) failed: %s", sqlite3_errmsg(db->db));
        if (sqlite3_reset(db->insert_mouse_press_stmt) != SQLITE_OK) {
            error("sqlite3_reset(insert_mouse_press) failed: %s", sqlite3_errmsg(db->db));
        }
        return 1;
    }

    if (sqlite3_reset(db->insert_mouse_press_stmt) != SQLITE_OK) {
        error("sqlite3_reset(insert_mouse_press) failed: %s", sqlite3_errmsg(db->db));
        return 1;
    }


    return 0;
}

int database_insert_batch(struct database *db, union event *buf, size_t len) {
    debug("inserting batch size=%d", len);

    if (sqlite3_step(db->begin_stmt) != SQLITE_DONE) {
        error("sqlite3_step(begin) failed: %s", sqlite3_errmsg(db->db));
        return 1;
    }
    if (sqlite3_reset(db->begin_stmt) != SQLITE_OK) {
        error("sqlite3_reset(begin) failed: %s", sqlite3_errmsg(db->db));
        return 1;
    }

    for (size_t i = 0; i < len; i++) {
        switch (buf[i].type) {
        case EVT_KEY:
            if (insert_single_key(db, buf[i])) {
                error("failed to insert record %zu", i);
                goto rollback;
            }
            break;

        case EVT_MOUSE:
            if (insert_single_mouse(db, buf[i])) {
                error("failed to insert record %zu", i);
                goto rollback;
            }
        }
    }

    if (sqlite3_step(db->commit_stmt) != SQLITE_DONE) {
        error("sqlite3_step(commit) failed: %s", sqlite3_errmsg(db->db));
        goto rollback;
    }
    if (sqlite3_reset(db->commit_stmt) != SQLITE_OK) {
        error("sqlite3_reset(commit) failed: %s", sqlite3_errmsg(db->db));
        return 1;
    }

    return 0;

rollback:
    if (sqlite3_step(db->rollback_stmt) != SQLITE_DONE) {
        error("sqlite3_step(rollback) failed: %s", sqlite3_errmsg(db->db));
    } else if (sqlite3_reset(db->rollback_stmt) != SQLITE_OK) {
        error("sqlite3_reset(rollback) failed: %s", sqlite3_errmsg(db->db));
    }
    return 1;
}

sqlite3 *database_get_raw(struct database *db) { return db->db; }
