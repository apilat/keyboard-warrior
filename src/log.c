#include "log.h"

#include <stdarg.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <time.h>

static atomic_int global_level;

void set_log_level(enum log_level level) {
    atomic_store_explicit(&global_level, level, memory_order_release);
}
enum log_level get_log_level() { return atomic_load_explicit(&global_level, memory_order_acquire); }
const char *level_to_string(enum log_level level) {
    switch (level) {
    case level_trace:
        return "TRACE";
    case level_debug:
        return "DEBUG";
    case level_info:
        return "INFO";
    case level_warn:
        return "WARN";
    case level_error:
        return "ERROR";
    default:
        return "?";
    }
}
enum log_level string_to_level(char *s) {
    if (strcasecmp(s, "TRACE") == 0) {
        return level_trace;
    } else if (strcasecmp(s, "DEBUG") == 0) {
        return level_debug;
    } else if (strcasecmp(s, "INFO") == 0) {
        return level_info;
    } else if (strcasecmp(s, "WARN") == 0) {
        return level_warn;
    } else if (strcasecmp(s, "ERROR") == 0) {
        return level_error;
    } else {
        return 0;
    }
}

void log_write(enum log_level level, char *file, int line, char *fmt, ...) {
    if (level < get_log_level()) {
        return;
    }

#ifdef __linux__
    flockfile(stderr);
#endif
    fprintf(stderr, "[%s]@(%s:%d) ", level_to_string(level), file, line);

    struct timespec tv;
    if (clock_gettime(CLOCK_REALTIME, &tv)) {
        fprintf(stderr, "(      <cannot get time>      ): ");
    } else {
        struct tm *tm = localtime(&tv.tv_sec);
        fprintf(stderr, "(%04d-%02d-%02d %02d:%02d:%02d.%06ld): ", tm->tm_year + 1900,
                tm->tm_mon + 1, tm->tm_mday, tm->tm_hour, tm->tm_min, tm->tm_sec,
                tv.tv_nsec / 1000);
    }

    va_list args;
    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);

    fputc('\n', stderr);
#ifdef __linux__
    funlockfile(stderr);
#endif
}
