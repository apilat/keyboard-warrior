#include "by_key.h"
#include "../const.h"
#include "../db.h"
#include "../log.h"
#include "../util.h"

#include <sqlite3.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct entry {
    // store keysym so we can quickly identify entry without string comparison
    uint32_t keycode;
    int64_t count;
    int bar_width;
};

struct aggregate {
    int max_keyname_width;
};

static int comp_entry_count(const void *e1_, const void *e2_) {
    const struct entry *e1 = e1_;
    const struct entry *e2 = e2_;
    return (e1->count < e2->count) - (e1->count > e2->count);
}

static int fetch_from_db(sqlite3 *db, struct entry **entries, size_t *entry_cnt) {
    sqlite3_stmt *stmt;

    if (sqlite3_prepare_v2(db,
                           "SELECT keycode, count(*)"
                           " FROM presses"
                           " GROUP BY keycode",
                           -1, &stmt, NULL) != SQLITE_OK) {
        error("sqlite3_prepare failed");
        return 1;
    }

    *entries = NULL;
    *entry_cnt = 0;
    size_t entry_cap = 0;

    int ret;
    while ((ret = sqlite3_step(stmt)) == SQLITE_ROW) {
        keycode code = sqlite3_column_int64(stmt, 0);
        int64_t count = sqlite3_column_int64(stmt, 1);

        buffer_push_uninit((void **)entries, sizeof(struct entry), entry_cnt, &entry_cap);
        struct entry *entry = &(*entries)[*entry_cnt - 1];
        entry->keycode = code;
        entry->count = count;
        entry->bar_width = 0;
    }

    if (ret != SQLITE_DONE) {
        error("sqlite3_step failed");
        return 1;
    }

    if (sqlite3_finalize(stmt) != SQLITE_OK) {
        error("sqlite3_finalize failed");
        return 1;
    }

    qsort(*entries, *entry_cnt, sizeof(**entries), comp_entry_count);

    return 0;
}

static int initialize_lines(int level, int entry_cnt, size_t *line_cnt, ssize_t **lines) {
    if (level >= 2 || entry_cnt <= BY_KEY_DEFAULT_COUNT * 2) {
        *line_cnt = entry_cnt;
        *lines = malloc_safe(*line_cnt * sizeof(**lines));

        for (size_t i = 0; i < *line_cnt; i++) {
            (*lines)[i] = i;
        }

    } else {
        *line_cnt = BY_KEY_DEFAULT_COUNT * 2 + 1;
        *lines = malloc_safe(*line_cnt * sizeof(**lines));

        for (size_t i = 0; i < BY_KEY_DEFAULT_COUNT; i++) {
            (*lines)[i] = i;
        }
        (*lines)[BY_KEY_DEFAULT_COUNT] = -1;
        for (size_t i = 0; i < BY_KEY_DEFAULT_COUNT; i++) {
            (*lines)[BY_KEY_DEFAULT_COUNT + 1 + i] = entry_cnt - BY_KEY_DEFAULT_COUNT + i;
        }
    }

    return 0;
}

static int process_aggregate(struct entry *entries, size_t entry_cnt, ssize_t *lines,
                             size_t line_cnt, struct aggregate *aggregate) {
    (void)entry_cnt;
    int max_keyname_width = 0;
    int64_t max_count = 0;

    for (size_t i = 0; i < line_cnt; i++) {
        if (lines[i] == -1) {
            continue;
        }
        struct entry *entry = &entries[lines[i]];
        if (entry->count > max_count) {
            max_count = entry->count;
        }
        const char *keyname = keycode_to_string(entry->keycode);
        int len = keyname == NULL ? 0 : strlen(keyname);
        if (len > max_keyname_width) {
            max_keyname_width = len;
        }
    }

    struct terminal_size size = get_terminal_size();
    ssize_t max_bar_width = size.cols - max_keyname_width - 1 - 6 - 2;
    if (max_bar_width < 0)
        max_bar_width = 0;

    for (size_t i = 0; i < line_cnt; i++) {
        if (lines[i] == -1) {
            continue;
        }

        struct entry *entry = &entries[lines[i]];
        entry->bar_width = max_bar_width * entry->count / max_count;
    }

    aggregate->max_keyname_width = max_keyname_width;

    return 0;
}

static void print_lines(struct entry *entries, size_t entry_cnt, ssize_t *lines, size_t line_cnt,
                        struct aggregate aggregate) {
    (void)entry_cnt;
    for (size_t i = 0; i < line_cnt; i++) {
        if (lines[i] == -1) {
            puts(" ...");
        } else {
            struct entry entry = entries[lines[i]];
            printf("%-*s %6ld ", aggregate.max_keyname_width, keycode_to_string(entry.keycode),
                   entry.count);
            printf("\e[48;2;255;255;255m%-*s\e[0m\n", entry.bar_width, "");
        }
    }
}

int print_counts_by_key(sqlite3 *db, int level) {
    struct entry *entries = NULL;
    size_t entry_cnt = 0;
    if (fetch_from_db(db, &entries, &entry_cnt)) {
        fprintf(stderr, "failed to fetch counts by key from database\n");
        return 1;
    }

    ssize_t *lines = NULL;
    size_t line_cnt = 0;
    if (initialize_lines(level, entry_cnt, &line_cnt, &lines)) {
        fprintf(stderr, "failed to determine which lines to print\n");
        goto error;
    }

    struct aggregate aggregate;
    if (process_aggregate(entries, entry_cnt, lines, line_cnt, &aggregate)) {
        fprintf(stderr, "failed to process aggregate data\n");
        goto error;
    }

    puts("> Counts by key");
    print_lines(entries, entry_cnt, lines, line_cnt, aggregate);
    putchar('\n');

    int ret = 0;
free:
    free(lines);
    free(entries);
    return ret;

error:
    ret = 1;
    goto free;
}
