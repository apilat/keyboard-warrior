#include "by_day.h"
#include "../const.h"
#include "../log.h"
#include "../util.h"

#include <sqlite3.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

struct date {
    int year, month, day;
};

struct entry {
    int day;
    int64_t total_count;
    // counts[subdivision]
    int64_t *counts;
};

struct aggregate {
    size_t subdivisions;
    int64_t max_single_count;
};

// Only guaranteed to work for years in 2000-2099.
static struct date get_date_from_day(int day) {
    day = day - 10958; // subtract 2000 offset
    int yearsx4 = day / (4 * 365 + 1);
    day = day % (4 * 365 + 1);
    int extra_years = day / 365;
    day = day % 365;
    const int days_per_month[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    int month = 0;
    while (day >= days_per_month[month]) {
        day -= days_per_month[month];
        month += 1;
    }

    return (struct date){2000 + yearsx4 * 4 + extra_years, month + 1, day + 1};
}

static int determine_day_range(int verbosity_level, int64_t *start_time, int64_t *end_time) {
    time_t cur_time = time(NULL);
    if (cur_time == -1) {
        error("time failed");
        return 1;
    }
    *end_time = (int64_t)cur_time * 1000000LL;

    if (verbosity_level >= 2) {
        *start_time = 0;
    } else {
        *start_time = *end_time - BY_DAY_DEFAULT_COUNT * 86400000000LL + 1;
    }

    return 0;
}

static int initialize_aggregate(struct aggregate *aggregate) {
    struct terminal_size size = get_terminal_size();
    ssize_t subdivisions_ = (ssize_t)size.cols - 10 - 1 - 6 - 2;
    aggregate->subdivisions = subdivisions_ > 0 ? subdivisions_ : 1;

    return 0;
}

static void free_entries(struct entry *entries, size_t entry_cnt, struct aggregate aggregate) {
    (void)aggregate;
    for (size_t i = 0; i < entry_cnt; i++) {
        free(entries[i].counts);
    }
    free(entries);
}

// Fills in entries
static int fetch_from_db(int64_t start_time, int64_t end_time, sqlite3 *db, struct entry **entries,
                         size_t *entry_cnt, size_t *entry_cap, struct aggregate *aggregate) {
    sqlite3_stmt *stmt;
    if (sqlite3_prepare_v2(db,
                           "SELECT timestamp * ? / 86400000000 AS flat_time, count(*)"
                           " FROM presses WHERE ? <= timestamp AND timestamp < ?"
                           " GROUP BY flat_time",
                           -1, &stmt, NULL) != SQLITE_OK ||
        sqlite3_bind_int64(stmt, 1, aggregate->subdivisions) != SQLITE_OK ||
        sqlite3_bind_int64(stmt, 2, start_time) != SQLITE_OK ||
        sqlite3_bind_int64(stmt, 3, end_time) != SQLITE_OK) {
        error("sqlite3_prepare or sqlite3_bind failed");
        return 1;
    }

    *entries = NULL;
    *entry_cnt = 0;
    *entry_cap = 0;
    size_t entry_ptr = 0;

    int ret;
    while ((ret = sqlite3_step(stmt)) == SQLITE_ROW) {
        int64_t time = sqlite3_column_int64(stmt, 0);
        int64_t count = sqlite3_column_int64(stmt, 1);

        int day = time / aggregate->subdivisions;
        int entry = time % aggregate->subdivisions;

        // We rely on the fact that timestamps are mostly sorted in the natural database order. This
        // improves the complexity of this approach from a naive quadratic to linear.

        while (entry_ptr < *entry_cnt && (*entries)[entry_ptr].day < day)
            entry_ptr++;
        while (entry_ptr > 0 && (*entries)[entry_ptr - 1].day >= day)
            entry_ptr--;
        // Find min entry_ptr such that (*entries)[entry_ptr].day >= day

        struct entry new_entry = (struct entry){
            .day = day,
            .total_count = 0,
            .counts = malloc_safe(sizeof(new_entry.counts[0]) * aggregate->subdivisions)};
        memset(new_entry.counts, 0, sizeof(new_entry.counts[0]) * aggregate->subdivisions);

        if (entry_ptr == *entry_cnt || (*entries)[entry_ptr].day > day) {
            // This is very bad if timestamps happen to be decreasing but this shouldn't happen
            // in normal functioning.
            buffer_push((void **)entries, sizeof(new_entry), entry_cnt, entry_cap, &new_entry);
            for (size_t j = *entry_cnt - 1; j > entry_ptr; j--) {
                (*entries)[j] = (*entries)[j - 1];
            }
            (*entries)[entry_ptr] = new_entry;
        }

        (*entries)[entry_ptr].total_count += count;
        (*entries)[entry_ptr].counts[entry] = count;
    }

    if (ret != SQLITE_DONE || sqlite3_finalize(stmt) != SQLITE_OK) {
        error("sqlite3_step or sqlite3_finalize failed");
        return 1;
    }

    return 0;
}

static int calculate_aggregate(struct entry *entries, size_t entry_cnt,
                               struct aggregate *aggregate) {
    int64_t max_count = 0;
    for (size_t i = 0; i < entry_cnt; i++) {
        for (size_t j = 0; j < aggregate->subdivisions; j++) {
            int64_t cnt = entries[i].counts[j];
            if (cnt > max_count) {
                max_count = cnt;
            }
        }
    }
    aggregate->max_single_count = max_count;
    return 0;
}

static void print_entries(struct entry *entries, size_t entry_cnt, struct aggregate aggregate) {
    int prev_day;

    for (size_t i = 0; i < entry_cnt; i++) {
        struct entry entry = entries[i];
        struct date date = get_date_from_day(entry.day);

        if (i > 0 && entry.day > prev_day + 1) {
            if (entry.day <= prev_day + 4) { // <= 3 day gap
                for (int missed_day = prev_day + 1; missed_day < entry.day; missed_day++) {
                    struct date missed_date = get_date_from_day(missed_day);
                    printf("%04d/%02d/%02d %6ld\n", missed_date.year, missed_date.month,
                           missed_date.day, 0L);
                }
            } else {
                printf("  ... (%d days)\n", entry.day - prev_day - 1);
            }
        }
        prev_day = entry.day;

        printf("%04d/%02d/%02d %6ld ", date.year, date.month, date.day, entry.total_count);

        if (aggregate.subdivisions == 1) {
            putchar('\n');
            continue;
        }

        for (size_t j = 0; j < aggregate.subdivisions; j++) {
            int64_t count = entry.counts[j];
            if (count == 0) {
                printf("\e[0m ");
            } else {
                uint8_t rgb[3] = {(uint64_t)255 * count / aggregate.max_single_count,
                                  (uint64_t)255 * count / aggregate.max_single_count,
                                  (uint64_t)255 * count / aggregate.max_single_count};
                printf("\e[48;2;%d;%d;%dm ", rgb[0], rgb[1], rgb[2]);
            }
        }
        puts("\e[0m");
    }
}

int print_counts_by_day(sqlite3 *db, int level) {
    int64_t start_time, end_time;
    if (determine_day_range(level, &start_time, &end_time)) {
        error("failed to determine day range");
        return 1;
    }

    struct entry *entries = NULL;
    size_t entry_cnt = 0, entry_cap = 0;
    struct aggregate aggregate;
    if (initialize_aggregate(&aggregate)) {
        error("failed to initialize aggregate");
        return 1;
    }

    if (fetch_from_db(start_time, end_time, db, &entries, &entry_cnt, &entry_cap, &aggregate)) {
        error("failed to fetch counts from db");
        return 1;
    }

    if (calculate_aggregate(entries, entry_cnt, &aggregate)) {
        error("failed to calculate aggregate stats");
        return 1;
    }

    puts("> Counts by day");
    print_entries(entries, entry_cnt, aggregate);
    putchar('\n');

    free_entries(entries, entry_cnt, aggregate);
    return 0;
}
