#ifndef WARRIOR_STATS
#define WARRIOR_STATS

struct database;

int stats_print_overview(struct database *db, int level);

#endif
