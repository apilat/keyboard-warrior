#include "../db.h"
#include "by_day.h"
#include "by_key.h"

#include <sqlite3.h>
#include <stdio.h>

static int print_total_count(sqlite3 *db, int level) {
    (void)level;
    sqlite3_stmt *stmt;

    if (sqlite3_prepare_v2(db, "SELECT count(*) FROM presses", -1, &stmt, NULL) != SQLITE_OK ||
        sqlite3_step(stmt) != SQLITE_ROW) {
        fprintf(stderr, "failed to get keypress count\n");
        return 1;
    }

    int64_t count = sqlite3_column_int64(stmt, 0);

    if (sqlite3_step(stmt) != SQLITE_DONE || sqlite3_finalize(stmt) != SQLITE_OK) {
        fprintf(stderr, "failed to get keypress count\n");
        return 1;
    }

    printf("> Total count = %ld\n\n", count);

    return 0;
}

int stats_print_overview(struct database *db_, int level) {
    sqlite3 *db = database_get_raw(db_);

    if (print_total_count(db, level) || print_counts_by_key(db, level) ||
        print_counts_by_day(db, level)) {
        return 1;
    } else {
        return 0;
    }
}
