#ifndef WARRIOR_MIGRATION
#define WARRIOR_MIGRATION

#include <stdbool.h>

typedef struct sqlite3 sqlite3;
int migrate_version(sqlite3 *db, bool readonly);

#endif
