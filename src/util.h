#ifndef WARRIOR_UTIL
#define WARRIOR_UTIL

#include <unistd.h>
#include <stdbool.h>
#include <stdint.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

struct terminal_size {
    int rows, cols;
};

struct terminal_size get_terminal_size();

void* malloc_safe(size_t size);
void* realloc_safe(void* ptr, size_t size);

bool buffer_push_uninit(void **buf, size_t esize, size_t *size, size_t *cap);
void buffer_push(void **buf, size_t esize, size_t *size, size_t *cap, void *elem);


char* get_default_database_file();

uint64_t randint();

int ignore_signals_pthread(void);

#endif
