# keyboard-warrior: daemon for tracking key press statistics

Note this is **not** a keylogger - the exact time and order of individual key presses is randomized to avoid leaking passwords and other sensitive data.
Instead, this tool is intended to gather more broad statistical data, allowing you to answer questions such as
"Which keys do I press the most?" or "Is there a difference in the distribution of keys between week days and weekends?".

## Features

* Linux (X11) and Windows support.
* Basic analysis is built-in.
All data is stored in an SQLite database so more advanced custom analysis can be performed externally.

## Usage

Run daemon to collect key presses: `warrior daemon --use-keys`

Retrieve basic statistics: `warrior stats`
